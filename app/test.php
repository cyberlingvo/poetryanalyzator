<?
include("app.php");
$text2 = "There will always be this place
inside
where I feel her absence
where I feel the echo of her lost voice –
the one she would have used to call me
back from sadness as she had to be
called so many times back from madness.

What would it take to summon her –
Not having an address, just a marker
for where she is not
I can only go and visit
her absence her remains
which become less and less like her
more and more like the earth and trees,
the sky she continually faces.
I’d rather picture her under the sea
hair waving to the fishes and the brine,
being washed clean by
sharks and plankton
than under those pines by
the stone bench: one more desiccating root
in a garden of bones.";


$text = "Всегда будет это место
внутри
где я чувствую ее отсутствие
где я чувствую эхо её пропавшего голоса –
одного, которым она раньше звала меня
назад от печали так, как её
звали так много раз назад от безумия.

Что потребовалось бы, чтобы её вызвать –
Не имея адреса, только знак,
для указания, где её нет
Я могу только пойти и посетить
Её отсутствие, её остатки,
которые становятся всё меньше и меньше похожие на неё,
все больше и больше похожими на землю и деревья,
небо, куда она непрерывно повернута.
Я бы лучше рисовала её под толщей моря,
с волосами, развевающимися среди рыб и рассола,
будучи помытой чисто
акулами и планктоном
чем под теми соснами
у каменной скамьи: еще один высушенный корень
в саду костей.";

$text3 = "Оно всегда будет
внутри
это место
там я чувствую её отсутствие
там я слышу эхо утраченного голоса
её голоса который звал меня когда-то прочь от тоски
как звали её сотни раз прочь от безумия.

И что теперь её вернет –
Нет адреса, одна зарубка,
там, где её уж нет
Там я могу лишь навестить
её небытие, останки,
всё менее близкие ей,
всё более схожие с почвой, деревьями,
небом, куда она глядится бесконечно.
Яснее вижу я её на дне,
где стайки рыб играют с волосами,
а остов драят добела
акулы и планктон,
чем там, под соснами
у каменной скамьи: еще один засохший корень
в саду костей.";

$p = new PoetryAnalyzer();

$counter1 = $p->analyze($text2, $text);
$counter2 = $p->analyze($text2, $text3);

$m = $p->getRuMorpho();

$o = $p->getEnMorpho();

foreach ($o['final_words'] as $row => $words) {
    ksort($words);
    foreach ($words as $index => $word) {
        echo $word['word'] . "(" . @implode(',', $word['tr']['exact']) . ") ";
    }

    echo "<br>";
}