<?
ini_set("display_errors", 1);
error_reporting(E_ALL);

include("settings.php");

class DB
{

    private $link;

    function __construct()
    {
        $this->link = new mysqli(DBHOST, DBUSER, DBPASS, DBBASE, 3306);
        /* изменение набора символов на utf8 */
        if (!mysqli_set_charset($this->link, "utf8")) {
            //printf("Ошибка при загрузке набора символов utf8: %s\n", mysqli_error($this->link));
        } else {
            //printf("Текущий набор символов: %s\n", mysqli_character_set_name($this->link));
        }
        if (mysqli_connect_errno()) {
            printf("Ошибка соединения: %s\n", mysqli_connect_error());
            exit();
        }
    }

    private function runSelectQuery($query)
    {


    }

    public function getPoetryList($start)
    {
        $query = "SELECT id, title, countersWFW, countersPoetry FROM poetry  ORDER by id DESC LIMIT $start, 10";

        $res = array();
        if ($result = mysqli_query($this->link, $query)) {

            while ($row = $result->fetch_assoc()) {

                $res[] = array(
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'countersWFW' => json_decode($row['countersWFW']),
                    'countersPoetry' => json_decode($row['countersPoetry'])
                );
            }

            $result->free();
        }

        return $res;
    }

    public function getSingle($id)
    {
        $query = "SELECT * FROM poetry WHERE id=" . $id;

        $res = array();
        if ($result = mysqli_query($this->link, $query)) {

            while ($row = $result->fetch_assoc()) {

                $res = array(
                    'id' => $row['id'],
                    'title' => $row['title'],
                    'author' => $row['author'],
                    'originalText' => stripcslashes($row['originalText']),
                    'wfwText' => stripcslashes($row['wfwText']),
                    'poetryText' => stripcslashes($row['poetryText']),

                    'jsonWFW' => $row['jsonWFW'],
                    'jsonPoetry' => $row['jsonPoetry'],

                    'countersWFW' => json_decode($row['countersWFW']),
                    'countersPoetry' => json_decode($row['countersPoetry'])
                );
            }

            $result->free();
        }

        return $res;
    }

    public function deleteItem($id)
    {

        //prepared statement
        $query = "DELETE FROM poetry WHERE id=?";

        if ($stmt = mysqli_prepare($this->link, $query)) {

            mysqli_stmt_bind_param($stmt, "s", $id);

            /* запускаем запрос */
            mysqli_stmt_execute($stmt);

            /* закрываем запрос */
            mysqli_stmt_close($stmt);

            return true;
        } else
            return false;
    }

    public function addUpdateItem($title, $author, $originalText, $wfwText, $poetryText,
                                  $jsonWFW, $jsonPoetry,
                                  $counterWFW, $counterPoetry,
                                  $id = null, $update = true)
    {

        //prepared statement

        if ($update)
            $query = "UPDATE poetry SET title=?, author=?, originalText=?, wfwText=?, poetryText=?, jsonWFW=?, jsonPoetry=?, countersWFW=?, countersPoetry=? WHERE id=?";
        else
            $query = "INSERT INTO poetry(title, author, originalText, wfwText, poetryText, jsonWFW, jsonPoetry,countersWFW, countersPoetry) VALUES(?, ?, ?, ?, ?, ?, ? ,? ,?)";


        if ($stmt = mysqli_prepare($this->link, $query)) {


            /* связываем параметры с метками */

            if ($update) {

                mysqli_stmt_bind_param($stmt, "sssssssssi", $title, $author, $originalText, $wfwText, $poetryText,
                    $jsonWFW, $jsonPoetry,
                    $counterWFW, $counterPoetry, $id);
            } else
                mysqli_stmt_bind_param($stmt, "sssssssss", $title, $author, $originalText, $wfwText, $poetryText,
                    $jsonWFW, $jsonPoetry,
                    $counterWFW, $counterPoetry);

            /* запускаем запрос */
            mysqli_stmt_execute($stmt);

            /* закрываем запрос */
            mysqli_stmt_close($stmt);

            return true;
        } else
            return false;
    }

    public function getLastInsertedId()
    {
        return mysqli_insert_id($this->link);
    }

    public function searchSynonyms($word)
    {
        $query = "SELECT word
            FROM  synonyms s
            LEFT JOIN words w ON s.s_id = w.id
            WHERE s.w_id = (SELECT id as wid  FROM words WHERE word = ?)
            ";

        if ($stmt = mysqli_prepare($this->link, $query)) {


            mysqli_stmt_bind_param($stmt, "s", $word);

            /* запускаем запрос */
            mysqli_stmt_execute($stmt);

            $res = array();

            $stmt->bind_result($singleWord);
            while ($stmt->fetch()) {
                $res[]['word'] = $singleWord;

            }

            /* instead of bind_result: */
            //$result = $stmt->get_result();

            //if ($result) {
            //    while ($row = $result->fetch_assoc()) {
            //        $res[] = $row;
            //    }
            //    $result->free();
            //}

            /* закрываем запрос */
            mysqli_stmt_close($stmt);

            return $res;
        } else
            return false;
    }

    public function searchWord($word)
    {
        $query = "SELECT definition FROM dict WHERE keyword=? ORDER BY keyword, binary(keyword) DESC";

        if ($stmt = mysqli_prepare($this->link, $query)) {


            mysqli_stmt_bind_param($stmt, "s", $word);

            /* запускаем запрос */
            mysqli_stmt_execute($stmt);

            /* instead of bind_result: */
            //$result = $stmt->get_result();

            $stmt->bind_result($definition);
            $res = array();
            while ($stmt->fetch()) {
                $res[]['definition'] = $definition;

            }

            //print_r($res);
            //while ($row = $result->fetch_assoc()) {
            //    $res[] = $row;
            //}


            //$result->free();


            /* закрываем запрос */
            mysqli_stmt_close($stmt);

            return $res;
        } else
            return false;
    }


    function __destruct()
    {
        mysqli_close($this->link);
    }
}