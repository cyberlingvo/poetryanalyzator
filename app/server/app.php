<?
include("morpho.php");
include("db.php");

class PoetryAnalyzer
{

    //eng text
    private $originalText;
    //morpho obj results for the text
    private $originalMorpho;
    //coef counters
    private $originalCounters;

    //the same as above but for russian text
    private $rusTr;
    private $rusMorpho;
    private $rusCounters;

    //Morpho obj that performs an analysis of Russian text
    private $ruMorpho;

    //Morpho obj that performs an analysis of English text
    private $enMorpho;

    //database obj
    private $db;

    function __construct()
    {

        //initializing
        $this->ruMorpho = new Morpho(
            'rus',
            array('Г', 'КР_ПРИЧАСТИЕ', 'ИНФИНИТИВ', 'МС-П', 'Н', 'ЧИСЛ', 'П', 'МС', 'С', 'ДЕЕПРИЧАСТИЕ', 'ПРИЧАСТИЕ'),
            array('СОЮЗ', 'ПРЕДЛ', 'МЕЖД')
        );
        $this->enMorpho = new Morpho('eng', array('ADVERB', 'ADJECTIVE', 'PN', 'NOUN', 'VERB', 'PN_ADJ'), array(), true);

        $this->db = new DB("localhost", "admin_varya", "mega", "admin_varya");

    }

    public function analyze($o, $rusText)
    {
        $this->originalText = $o;
        $this->rusTr = $rusText;

        $this->originalMorpho = $this->enMorpho->analyze($this->originalText);
        $this->rusMorpho = $this->ruMorpho->analyze($this->rusTr);

        //translate all important words from eng to rus
        $this->prepareTranslation();

        return $this->getCounters();
        //
        //print_r($this->originalMorpho);
        //print_r($this->rusMorpho);

        //foreach ($this->originalMorpho['final_words'] as $row => $words) {
        //    ksort($words);
        //    foreach ($words as $index => $word) {
        //        echo $word['word'] . "(" . @implode(',', $word['tr']['synonym']) . ") ";
        //    }
        //
        //    echo "<br>";
        //}
    }

    //translate eng words using LingvoEnUniversal dictionary (~100 000 words)
    private function prepareTranslation()
    {
        $this->originalMorpho['translated'] = array();

        foreach ($this->originalMorpho['final_words'] as $row => $words) {
            foreach ($words as $index => $word) {

                if (!$word['is_important'])
                    continue;

                $this->originalMorpho['translated'][$row][$index] = array();

                $defs = array();

                foreach ($word['data']['forms'] as $form) {

                    $vb = $this->db->searchWord(mb_strtolower($form, 'UTF-8'));

                    if (count($vb) > 0) {

                        //remove unnecessary tags
                        $prep = $this->prepareTags($vb[0]['definition']);
                        //get definitions that are inside <dtrn> tag
                        $tags = $this->getTextBetweenTags($prep, 'dtrn');

                        //add all definitions to the word data that just has been translated
                        foreach ($tags as $tag) {

                            $def = preg_split("/[,;]/", $tag);

                            foreach ($def as $v) {
                                $term = trim($v);


                                if ($term != '') {

                                    $defs[] = $term;

                                }

                            }

                        }

                        //if (empty($definitions)){
                        //    echo $word['word'];
                        //    print_r($vb);
                        //}


                    } else {
                        //if nothing in db, then just add this word (to be analyzed later)
                        $this->originalMorpho['translated'][$row][$index] = $word['word'];
                    }
                }

                $this->originalMorpho['translated'][$row][$index] = $defs;
            }
        }
    }


    /**
     * Trying to find orignial word in eng text for russian word in a $i-th row
     *
     * @param $i - row to be checked
     * @param $keyToCheck - array to be checked (forms or synonyms). 'forms' is array with the list of all possible base forms for the word
     * @param $word - word data item. Word itself is in <code>$word['word']</code>
     * @param $exact - counter(by link) for words translated exactly (using definition from database dictionary)
     * @param $syn  - counter(by link) for words translated using list of synonyms (that is obtained from database dictionary)
     * @param $isFound - flag indicating that translation has been found (by link)
     * @param $wordPos - location (row, position) of the russian word to be identified in english text
     * @param $foundPos - location (row, position) of the identified english word (by link)
     * @param bool $isExact - flag indicating whether we are looking for exact translation or for translation of synonyms
     */
    private function checkRow($i, $keyToCheck, $word, &$exact, &$syn, &$isFound, $wordPos, &$foundPos, $isExact = true)
    {

        if (!$isFound && isset($this->originalMorpho['translated'][$i])) {
            $wordsToCheck = $this->originalMorpho['translated'][$i];

            foreach ($word['data'][$keyToCheck] as $form) {

                $term = mb_strtolower($form, 'UTF-8');

                if ($isFound)
                    break;

                foreach ($wordsToCheck as $wi => $terms) {

                    if (isset($this->originalMorpho['final_words'][$i][$wi]['tr']['exact']))
                        continue;

                    if (false !== $index = array_search($term, $terms)) {

                        $isFound = true;

                        $foundPos = array('row' => $i, 'pos' => $wi);

                        if ($isExact) {
                            $exact++;
                            $this->originalMorpho['final_words'][$i][$wi]['tr']['exact'] = $wordPos;
                            $this->rusMorpho['final_words'][$wordPos['row']][$wordPos['pos']]['exact'] = $foundPos;
                        } else {
                            $syn++;
                            $this->originalMorpho['final_words'][$i][$wi]['tr']['synonym'] = $wordPos;
                            $this->rusMorpho['final_words'][$wordPos['row']][$wordPos['pos']]['synonym'] = $foundPos;
                        }

                        break;
                    }


                }
            }


        }

    }

    private function getCounters()
    {
        $exact = 0;
        $syn = 0;

        //check for exact
        foreach ($this->rusMorpho['final_words'] as $row => $words) {
            ksort($words);
            foreach ($words as $index => $word) {

                if (!$word['is_important'])
                    continue;

                $is_found = false;

                $wordPos = array('row' => $row, 'pos' => $index);
                $foundPos = array();

                //trying to find original word in the rows from $current - 2 to $current + 2
                $this->checkRow($row, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row + 1, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row - 1, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row + 2, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row - 2, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);

            }
        }

        //check for synonyms  (almost the same)
        //we are doing it here in order not to query db too much

        foreach ($this->rusMorpho['final_words'] as $row => $words) {
            ksort($words);
            foreach ($words as $index => $word) {

                if (!$word['is_important'] || isset($this->rusMorpho['final_words'][$row][$index]['exact']))
                    continue;

                $is_found = false;

                $synonyms = array();

                foreach ($word['data']['forms'] as $form) {
                    $synonymsList = $this->db->searchSynonyms(mb_strtolower($form, 'UTF-8'));

                    foreach ($synonymsList as $dbRow) {
                        $synonyms[] = $dbRow['word'];
                    }
                }


                $wordPos = array('row' => $row, 'pos' => $index);
                $foundPos = array();

                $word['data']['synonyms'] = $synonyms;

                $this->checkRow($row, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row + 1, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row - 1, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row + 2, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row - 2, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);

            }
        }

        $this->originalCounters = array($this->originalMorpho['importantCounter'], $this->originalMorpho['importantCounter'] - $exact - $syn);
        $this->rusCounters = array($this->rusMorpho['importantCounter'], $exact, $syn, $this->rusMorpho['importantCounter'] - $exact - $syn);

        return array($this->originalCounters, $this->rusCounters);

        //foreach ($this->originalMorpho['final_words'] as $row => $words) {
        //    foreach ($words as $index => $word) {
        //
        //        if ($word['is_important'] && !isset($word['tr'])) {
        //            echo "<hr>" . $word['word'];
        //        }
        //    }
        //}
    }

    private function getTextBetweenTags($string, $tagname)
    {
        $pattern = "/<$tagname>(.*?)<\/$tagname>/";
        preg_match_all($pattern, $string, $matches);
        return $matches[1];
    }

    private function prepareTags($definition)
    {
        $res = preg_replace("/<c>.+?<\/c>/i", "", $definition);
        $res = preg_replace("/&quot.+?&quot/i", "", $res);
        $res = preg_replace("/<co>.+?<\/co>/i", "", $res);
        $res = preg_replace("/<abr>.+?<\/abr>/i", "", $res);
        $res = preg_replace("/<kref>.+?<\/kref>/i", "", $res);
        $res = preg_replace("/<i>.+?<\/i>/i", "", $res);
        $res = str_replace(array('!', '?', '<i></i>', '<c></c>'), '', $res);
        $res = preg_replace("/\([^)]+\)/", "", $res);
        $res = trim($res);

        return $res;
    }

}

//$text2 = "   I'm lazybone and my name is Tom, unlucky cat, a discordant tone,
//          The greatest talent to catch a stone with my unsuccessful head.
//          Your name is Jerry, a little trash, you have a skill to contrive a crash
//          Upon my head, you are vile and brash, and sometimes a little mad.
//
//          We’ve settled down in Texas state, somewhere on Houston tectonic plate,
//          And viewers make us a perfect rate from five then to six o’clock.
//          You tear my moustache out and I entice your nose with a poisoned pie,
//          We’re bored with it, but we are to vie: spectators don’t think it’s mock.
//
//          The yard is littered with trip-wire mines, large-toothed cattraps, carnivorous bines,
//          But we are friends, cause we only mime this enmity, hatred, spite.
//          So each of us gets a weekly cheque, though a scriptwriter forgot to check
//          That Jerry’s a very short-lived chap, for him it’s a flying kite.
//
//          So hope’s concealed in a droll grimace, in every jump, the eternal race,
//          Without pathos in random phrase, in this everlasting fun.
//          You make a snare with a machine press, we turn a house in junk and mess,
//          We run: it seems that there is no death we are therefore we run ";
//
//$text = ' Я буду, конечно, бездельник Том — не самый удачливый из котов,
//          Умеющий вляпаться, как никто, в какой-нибудь переплёт.
//          Ты будешь Джерри — грызун и дрянь, известный умением кинуть в грязь
//          И изворотливостью угря; коварный, как первый лёд.
//
//          Мы будем жить для отвода глаз в каком-нибудь Хьюстоне, штат Техас,
//          И зрители будут смотреть на нас с пяти часов до шести.
//          Ты выдираешь мои усы, я сыплю мышьяк в твой швейцарский сыр,
//          И каждый из нас этим, в общем, сыт, но шоу должно идти.
//
//          Весь двор в растяжках и язвах ям, вчера я бросил в тебя рояль,
//          Но есть подтекст, будто мы друзья, а это всё — суета.
//          Нам раз в неделю вручают чек. Жаль, сценарист позабыл прочесть,
//          Что жизнь мышонка короче, чем... короче, чем жизнь кота.
//
//          Надежда — в смене смешных гримас, в прыжках, в ехидном прищуре глаз,
//          В отсутствии пафосных плоских фраз, в азарте, в гульбе, в стрельбе...
//          Ты сбрасываешь на меня буфет кричу от боли кидаюсь вслед
//          бегу и вроде бы смерти нет а есть только бег бег бег ';



//$text = "Оно всегда будет
//внутри
//это место
//там я чувствую её отсутствие
//там я слышу эхо утраченного голоса
//её голоса который звал меня когда-то прочь от тоски
//как звали её сотни раз прочь от безумия.
//
//И что теперь её вернет –
//Нет адреса, одна зарубка,
//там, где её уж нет
//Там я могу лишь навестить
//её небытие, останки,
//всё менее близкие ей,
//всё более схожие с почвой, деревьями,
//небом, куда она глядится бесконечно.
//Яснее вижу я её на дне,
//где стайки рыб играют с волосами,
//а остов драят добела
//акулы и планктон,
//чем там, под соснами
//у каменной скамьи: еще один засохший корень
//в саду костей.";


