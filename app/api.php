<?

if (!empty($_GET)) {
    $action = $_GET['action'];

    include("db.php");
    $db = new DB();

    switch ($action) {
        case "getList":

            $start = $_GET['start'];

            echo json_encode($db->getPoetryList($start));
            die();

            break;

        case 'getSingle':

            $id = intval($_GET['id']);
            $single = $db->getSingle($id);

            echo json_encode($single);
            die();

            break;

        case 'deleteSingle':

            $id = intval($_GET['id']);

            break;
    }
} else if (!empty($_POST)) {

    include("app.php");

    $db = new DB();

    function writeJsonFile($data)
    {

        $name = uniqid(rand(), true);
        $fp = fopen("./json/$name.json", "wb");
        fwrite($fp, $data);
        fclose($fp);

        return $name;
    }

    $action = $_POST['action'];
    $title = $_POST['data']['title'];
    $author = $_POST['data']['author'];
    $original = $_POST['data']['txtOriginal'];
    $wordForWord = $_POST['data']['txtWFW'];
    $poetryTranslation = $_POST['data']['txtPoetryTranslation'];

    $pA = new PoetryAnalyzer();

    $counterWFW = json_encode($pA->analyze($original, $wordForWord));
    $dataWFW = array(
        'morphoDataRu' => $pA->getRuMorpho(),
        'morphoDataEn' => $pA->getEnMorpho()
    );
    $jsonWFW = writeJsonFile(json_encode($dataWFW));


    $counterPoetry = json_encode($pA->analyze($original, $poetryTranslation));
    $dataPoetry = array(
        'morphoDataRu' => $pA->getRuMorpho(),
        'morphoDataEn' => $pA->getEnMorpho()
    );

    $jsonPoetry = writeJsonFile(json_encode($dataPoetry));

    switch ($action) {
        case "update":

            $id = intval($_POST['id']);

            if ($db->addUpdateItem($title, $author, $original, $wordForWord, $poetryTranslation,
                $jsonWFW, $jsonPoetry,
                $counterWFW, $counterPoetry,
                $id, true)
            ) {

                echo json_encode(array('result' => '1', 'id' => $id));
                die();
            } else {
                echo json_encode(array('result' => '0'));
                die();
            }

            break;

        case "add":

            if ($db->addUpdateItem($title, $author, $original, $wordForWord, $poetryTranslation,
                $jsonWFW, $jsonPoetry,
                $counterWFW, $counterPoetry,
                null, false)
            ) {

                echo json_encode(array('result' => '1', 'id' => $db->getLastInsertedId()));
                die();
            } else {
                echo json_encode(array('result' => '0'));
                die();
            }


            break;
    }

}