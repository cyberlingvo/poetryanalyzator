/**
 * Main AngularJS Web Application
 */
var app = angular.module('app', [
    'ngRoute'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider

        .when("/", {templateUrl: "tpl/form.html", controller: "MainCtrl"})
        .when("/view/:id", {templateUrl: "tpl/view.html", controller: "PageCtrl"})
        .when("/edit/:id", {templateUrl: "tpl/form.html", controller: "MainCtrl"})

        .otherwise("/404", {templateUrl: "partials/404.html", controller: "PageCtrl"});

}]).filter('nl2br', ['$sce', function ($sce) {

    return function (text) {
        return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
    };

}]).run(function ($http, $rootScope) {

    $rootScope.poetries = [];
    $rootScope.start = 0;

    $rootScope.getList = function () {
        $http.get('api.php?action=getList&start=' + $rootScope.start).then(function (resp) {
            $rootScope.poetries = angular.fromJson(resp.data);
            console.log($rootScope.poetries)
        }, function () {
            alert('Не удалось загрузить базу стихов, попробуйте обновить страницу');
        });
    };

    $rootScope.getAdequacyCoef = function (p) {

        if (p === undefined || p.countersWFW === undefined)
            return 0;

        var coef = p.countersWFW[1][1] + p.countersPoetry[1][1] + p.countersWFW[1][2] + p.countersPoetry[1][2];
        coef /= (p.countersWFW[1][0] + p.countersPoetry[1][0]);
        coef -= (p.countersPoetry[0][1]) / (p.countersWFW[1][0]);

        return Math.abs(coef.toFixed(2));
    };

    $rootScope.getList();

});

app.controller('MainCtrl', function ($scope, $location, $http, $rootScope, $routeParams) {

    $rootScope.action = 'Добавить';
    $rootScope.activeId = -1;

    $rootScope.dataModel = {
        title: '',
        author: '',
        txtOriginal: '',
        txtWFW: '',
        txtPoetryTranslation: ''
    };

    $rootScope.saving = false;
    $rootScope.saveBtnTxt = 'Сохранить';


    if ($routeParams.id !== undefined) {

        $rootScope.activeId = $routeParams.id;

        $rootScope.action = 'Редактировать';

        $http.get('api.php?action=getSingle&id=' + $routeParams.id).then(function (resp) {
            $scope.poem = angular.fromJson(resp.data);

            $rootScope.dataModel = {
                title: $scope.poem.title,
                author: $scope.poem.author,
                txtOriginal: $scope.poem.originalText,
                txtWFW: $scope.poem.wfwText,
                txtPoetryTranslation: $scope.poem.poetryText
            };

        }, function () {
            alert('Не удалось загрузить данные');
        });
    }

    $rootScope.save = function () {
        $rootScope.saving = true;
        $rootScope.saveBtnTxt = 'Происходит анализ текста и сохранение в базу данных...';

        var data = {
            action: $routeParams.id !== undefined ? 'update' : 'add',
            data: $rootScope.dataModel
        };

        if ($routeParams.id !== undefined)
            data.id = $routeParams.id;

        $http({
            method: 'POST',
            url: 'api.php',
            data: $.param(data),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}

        }).success(function (res) {

            res = angular.fromJson(res);

            $rootScope.getList();

            $rootScope.saving = false;
            $rootScope.saveBtnTxt = 'Сохранить';

            if (res.result == 0) {
                alert('Не удалось сохранить информацию. Попробуйте еще раз');
            } else
                $location.path('/view/' + res.id);

        }).error(function (error) {
            alert('Не удалось сохранить информацию. Попробуйте еще раз');
        });


    };
});

app.controller('PageCtrl', function ($scope, $location, $http, $rootScope, $routeParams) {

    $scope.poem = {};

    $rootScope.activeId = $routeParams.id;


    $http.get('api.php?action=getSingle&id=' + $routeParams.id).then(function (resp) {

        $scope.poem = angular.fromJson(resp.data);

    }, function () {
        alert('Не удалось загрузить данные');
    });

});
