<?
include("morpho.php");
include("db.php");

class PoetryAnalyzer
{

    //eng text
    private $originalText;
    //morpho obj results for the text
    private $originalMorpho;
    //coef counters
    private $originalCounters;

    //the same as above but for russian text
    private $rusTr;
    private $rusMorpho;
    private $rusCounters;

    //Morpho obj that performs an analysis of Russian text
    private $ruMorpho;

    //Morpho obj that performs an analysis of English text
    private $enMorpho;

    //database obj
    private $db;

    function __construct()
    {

        //initializing
        $this->ruMorpho = new Morpho(
            'rus',
            array('Г', 'КР_ПРИЧАСТИЕ', 'ИНФИНИТИВ', 'МС-П', 'Н', 'ЧИСЛ', 'П', 'МС', 'С', 'ДЕЕПРИЧАСТИЕ', 'ПРИЧАСТИЕ'),
            array('СОЮЗ', 'ПРЕДЛ', 'МЕЖД')
        );
        $this->enMorpho = new Morpho('eng', array('ADVERB', 'ADJECTIVE', 'PN', 'NOUN', 'VERB', 'PN_ADJ'), array(), true);

        $this->db = new DB();

    }

    public function getRuMorpho(){
        return $this->rusMorpho;
    }

    public function getEnMorpho(){
        return $this->originalMorpho;
    }

    public function analyze($o, $rusText)
    {
        $this->originalText = $o;
        $this->rusTr = $rusText;

        $this->originalMorpho = $this->enMorpho->analyze($this->originalText);
        $this->rusMorpho = $this->ruMorpho->analyze($this->rusTr);

        //translate all important words from eng to rus
        $this->prepareTranslation();

        return $this->getCounters();
        //
        //print_r($this->originalMorpho);
        //print_r($this->rusMorpho);

        //foreach ($this->originalMorpho['final_words'] as $row => $words) {
        //    ksort($words);
        //    foreach ($words as $index => $word) {
        //        echo $word['word'] . "(" . @implode(',', $word['tr']['synonym']) . ") ";
        //    }
        //
        //    echo "<br>";
        //}
    }

    //translate eng words using LingvoEnUniversal dictionary (~100 000 words)
    private function prepareTranslation()
    {
        $this->originalMorpho['translated'] = array();

        foreach ($this->originalMorpho['final_words'] as $row => $words) {
            foreach ($words as $index => $word) {

                if (!$word['is_important'])
                    continue;

                $this->originalMorpho['translated'][$row][$index] = array();

                $defs = array();

                foreach ($word['data']['forms'] as $form) {

                    $vb = $this->db->searchWord(mb_strtolower($form, 'UTF-8'));

                    if (count($vb) > 0) {

                        //remove unnecessary tags
                        $prep = $this->prepareTags($vb[0]['definition']);
                        //get definitions that are inside <dtrn> tag
                        $tags = $this->getTextBetweenTags($prep, 'dtrn');

                        //add all definitions to the word data that just has been translated
                        foreach ($tags as $tag) {

                            $def = preg_split("/[,;]/", $tag);

                            foreach ($def as $v) {
                                $term = trim($v);


                                if ($term != '') {

                                    $defs[] = $term;

                                }

                            }

                        }

                        //if (empty($definitions)){
                        //    echo $word['word'];
                        //    print_r($vb);
                        //}


                    } else {
                        //if nothing in db, then just add this word (to be analyzed later)
                        $this->originalMorpho['translated'][$row][$index] = $word['word'];
                    }
                }

                $this->originalMorpho['translated'][$row][$index] = $defs;
            }
        }
    }


    /**
     * Trying to find orignial word in eng text for russian word in a $i-th row
     *
     * @param $i - row to be checked
     * @param $keyToCheck - array to be checked (forms or synonyms). 'forms' is array with the list of all possible base forms for the word
     * @param $word - word data item. Word itself is in <code>$word['word']</code>
     * @param $exact - counter(by link) for words translated exactly (using definition from database dictionary)
     * @param $syn  - counter(by link) for words translated using list of synonyms (that is obtained from database dictionary)
     * @param $isFound - flag indicating that translation has been found (by link)
     * @param $wordPos - location (row, position) of the russian word to be identified in english text
     * @param $foundPos - location (row, position) of the identified english word (by link)
     * @param bool $isExact - flag indicating whether we are looking for exact translation or for translation of synonyms
     */
    private function checkRow($i, $keyToCheck, $word, &$exact, &$syn, &$isFound, $wordPos, &$foundPos, $isExact = true)
    {

        if (!$isFound && isset($this->originalMorpho['translated'][$i])) {
            $wordsToCheck = $this->originalMorpho['translated'][$i];

            foreach ($word['data'][$keyToCheck] as $form) {

                $term = mb_strtolower($form, 'UTF-8');

                if ($isFound)
                    break;

                foreach ($wordsToCheck as $wi => $terms) {

                    if (isset($this->originalMorpho['final_words'][$i][$wi]['tr']['exact']))
                        continue;

                    if (false !== $index = array_search($term, $terms)) {

                        $isFound = true;

                        $foundPos = array('row' => $i, 'pos' => $wi);

                        if ($isExact) {
                            $exact++;
                            $this->originalMorpho['final_words'][$i][$wi]['tr']['exact'] = $wordPos;
                            $this->rusMorpho['final_words'][$wordPos['row']][$wordPos['pos']]['exact'] = $foundPos;
                        } else {
                            $syn++;
                            $this->originalMorpho['final_words'][$i][$wi]['tr']['synonym'] = $wordPos;
                            $this->rusMorpho['final_words'][$wordPos['row']][$wordPos['pos']]['synonym'] = $foundPos;
                        }

                        break;
                    }


                }
            }


        }

    }

    private function getCounters()
    {
        $exact = 0;
        $syn = 0;

        //check for exact
        foreach ($this->rusMorpho['final_words'] as $row => $words) {
            ksort($words);
            foreach ($words as $index => $word) {

                if (!$word['is_important'])
                    continue;

                $is_found = false;

                $wordPos = array('row' => $row, 'pos' => $index);
                $foundPos = array();

                //trying to find original word in the rows from $current - 2 to $current + 2
                $this->checkRow($row, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row + 1, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row - 1, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row + 2, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);
                $this->checkRow($row - 2, 'forms', $word, $exact, $syn, $is_found, $wordPos, $foundPos);

            }
        }

        //check for synonyms  (almost the same)
        //we are doing it here in order not to query db too much

        foreach ($this->rusMorpho['final_words'] as $row => $words) {
            ksort($words);
            foreach ($words as $index => $word) {

                if (!$word['is_important'] || isset($this->rusMorpho['final_words'][$row][$index]['exact']))
                    continue;

                $is_found = false;

                $synonyms = array();

                foreach ($word['data']['forms'] as $form) {
                    $synonymsList = $this->db->searchSynonyms(mb_strtolower($form, 'UTF-8'));

                    foreach ($synonymsList as $dbRow) {
                        $synonyms[] = $dbRow['word'];
                    }
                }


                $wordPos = array('row' => $row, 'pos' => $index);
                $foundPos = array();

                $word['data']['synonyms'] = $synonyms;

                $this->checkRow($row, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row + 1, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row - 1, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row + 2, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);
                $this->checkRow($row - 2, 'synonyms', $word, $exact, $syn, $is_found, $wordPos, $foundPos, false);

            }
        }

        $this->originalCounters = array($this->originalMorpho['importantCounter'], $this->originalMorpho['importantCounter'] - $exact - $syn);
        $this->rusCounters = array($this->rusMorpho['importantCounter'], $exact, $syn, $this->rusMorpho['importantCounter'] - $exact - $syn);

        return array($this->originalCounters, $this->rusCounters);

        //foreach ($this->originalMorpho['final_words'] as $row => $words) {
        //    foreach ($words as $index => $word) {
        //
        //        if ($word['is_important'] && !isset($word['tr'])) {
        //            echo "<hr>" . $word['word'];
        //        }
        //    }
        //}
    }

    private function getTextBetweenTags($string, $tagname)
    {
        $pattern = "/<$tagname>(.*?)<\/$tagname>/";
        preg_match_all($pattern, $string, $matches);
        return $matches[1];
    }

    private function prepareTags($definition)
    {
        $res = preg_replace("/<c>.+?<\/c>/i", "", $definition);
        $res = preg_replace("/&quot.+?&quot/i", "", $res);
        $res = preg_replace("/<co>.+?<\/co>/i", "", $res);
        $res = preg_replace("/<abr>.+?<\/abr>/i", "", $res);
        $res = preg_replace("/<kref>.+?<\/kref>/i", "", $res);
        $res = preg_replace("/<i>.+?<\/i>/i", "", $res);
        $res = str_replace(array('!', '?', '<i></i>', '<c></c>'), '', $res);
        $res = preg_replace("/\([^)]+\)/", "", $res);
        $res = trim($res);

        return $res;
    }

}