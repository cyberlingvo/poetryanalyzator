<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>СтихоАнализатор v.1.0</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body ng-app="app" ng-controller="MainCtrl" ng-cloak>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 header">

                <h1>
                    СтихоАнализатор v.1.0
                </h1>
                 <p>
                     Институт лингвистики
                 </p>
           <p>
               <b>Проект:</b>
               Особенности стихосложения и стиля в стихотворениях из сборника Шэрон Долин «Светотень» и их отражение в переводе
           </p>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <a class="btn btn-default" type="button" ng-href="#/">
                <em class="glyphicon glyphicon-plus"></em> Добавить
            </a>
            <hr/>

            <div class="list-group list">


                <a class="list-group-item" ng-repeat="poem in poetries" ng-href="#/view/{{poem.id}}" ng-class="{active: activeId === poem.id}">
                    <h4 class="list-group-item-heading">
                        {{poem.title}}
                    </h4>

                    <div class="list-group-item-text">

                        <p><small> Коэф для подстрочника: точность {{(poem.countersWFW[1][1] / poem.countersWFW[1][0]).toFixed(2) }},
                                вольность {{(poem.countersWFW[1][3] / poem.countersWFW[1][0]).toFixed(2)}}</small>
                        </p>

                        <p><small> Коэф для худ.перевода: точность {{ (poem.countersPoetry[1][1] / poem.countersPoetry[1][0]).toFixed(2) }},
                                вольность {{ (poem.countersPoetry[1][3] / poem.countersPoetry[1][0]).toFixed(2) }},
                                адекватность {{getAdequacyCoef(poem)}}
                            </small>
                        </p>

                    </div>
                </a>





            </div>
            <!--<ul class="pagination">-->
            <!--    <li>-->
            <!--        <a href="#">Prev</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">1</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">2</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">3</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">4</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">5</a>-->
            <!--    </li>-->
            <!--    <li>-->
            <!--        <a href="#">Next</a>-->
            <!--    </li>-->
            <!--</ul>-->
        </div>
        <div class="col-md-8" ng-view>

        </div>
    </div>

</div>
</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">&copy; 2015, Марченкова Варвара, "Особенности стихосложения и стиля в стихотворениях из сборника Шэрон Долин «Светотень» и их отражение в переводе"</p>
    </div>
</footer>

<script src="js/angular.min.js"></script>
<script src="js/angular-route.min.js"></script>
<script src="js/app.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>