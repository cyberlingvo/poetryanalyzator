<?
require_once(dirname(__FILE__) . '/../src/common.php');

class Morpho
{
    private $opts = array(
        // storage type, follow types supported
        // PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
        // PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
        // PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
        'storage' => PHPMORPHY_STORAGE_FILE,
        // Extend graminfo for getAllFormsWithGramInfo method call
        'with_gramtab' => false,
        // Enable prediction by suffix
        'predict_by_suffix' => true,
        // Enable prediction by prefix
        'predict_by_db' => true
    );

    // Path to directory where dictionaries located
    private $dir;

    // Create descriptor for dictionary located in $dir directory with russian language
    private $dict_bundle;

    //Main phpmprphy obj
    private $morphy;

    private $lang;

    //parts of speech that are counted as important
    private $allowedPoS;

    //parts of speech that are counted as non-important
    private $restrictedPoS;

    //whether to check or not for stopwords (only for eng now)
    private $checkForStopWords;

    private $stopWordsList = array(
        'eng' => array(
            'aboard', 'about', 'above', 'across', 'after', 'against', 'along', 'amid', 'among', 'anti', 'around', 'as', 'at',
            'before', 'behind', 'below', 'beneath', 'beside', 'besides', 'between', 'beyond', 'but', 'by',
            /*'concerning', 'considering',*/ 'despite',
            'down', 'during',
            //'except', 'excepting', 'excluding',
            /* 'following',*/ 'for', 'from',
            'in', /*'inside',*/ 'into',
            'like',
            //'minus',
            //'near',
            'of', 'off', 'on', 'onto',/* 'opposite', 'outside',*/ 'over',
            'past', 'per', 'plus',
            'regarding', 'round',
            /*'save',*/ 'since',
            'than', 'through', 'to', 'toward', 'towards',
            'under', 'underneath', 'unlike',
            'until', 'up', 'upon',
            'versus', 'via',
            'with', 'within', 'without'
        )
    );

    function __construct($lang, $allowedPoS, $restrictedPoS, $checkForStopWords = false)
    {
        $this->lang = $lang;

        $this->dir = dirname(__FILE__) . '/../dicts';

        $this->dict_bundle = new phpMorphy_FilesBundle($this->dir, $lang);

        $this->allowedPoS = $allowedPoS;

        $this->restrictedPoS = $restrictedPoS;

        $this->checkForStopWords = $checkForStopWords;

        try {

            $this->morphy = new phpMorphy($this->dict_bundle, $this->opts);

        } catch (phpMorphy_Exception $e) {
            die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
        }
    }


    private function prepateEngText($text)
    {
        $res = str_replace('’', '\'', $text);
        $res = str_replace('\'d', ' would', $res);
        $res = str_replace('\'ve', ' have', $res);
        $res = str_replace('\'ll', ' will', $res);
        $res = str_replace('wanna', 'want to', $res);
        $res = str_replace('gonna', 'going to', $res);

        return $res;
    }

    private function getWords($text, $alphabet = '')
    {
        $words = array();
        $rows = explode("\n", $text);
        $rowNumber = 0;

        foreach ($rows as $row) {
            $words[$rowNumber] = array();

            $row = str_replace("ё", "е", $row);

            if ($alphabet == '')
                $list = str_word_count($row, 1);
            else
                $list = str_word_count($row, 1, $alphabet);

            $index = 0;
            foreach ($list as $word) {
                $words[$rowNumber][$index] = $word;
                $index++;
            }

            $rowNumber++;
        }
        return $words;
    }

    private function endsWith($haystack, $needle)
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }

    public function analyze($text)
    {

        if ($this->lang == 'rus')
            $alphabet = "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя";
        else
            $alphabet = '';

        if ($this->lang == 'eng')
            $text = $this->prepateEngText($text);

        $finalWords = array();
        $importantCounter = 0;

        try {

            $words = $this->getWords($text, $alphabet);

            foreach ($words as $row => $rowWords) {

                $preparedWords = array();

                $finalWords[$row] = array();

                //Uppercasing all words (preparing them to be used by phpmorpher)
                foreach ($rowWords as $w) {
                    $preparedWords[] = mb_strtoupper($w, "UTF-8");
                }

                //getting base forms in bulk mode
                $base_form = $this->morphy->getBaseForm($preparedWords);

                foreach ($base_form as $word => $forms) {

                    //validating whether we got any forms
                    if ($word == '-' || count($forms) == 0 || $forms == '')
                        continue;

                    for ($i = 0; $i < count($words[$row]); $i++) {

                        $pOS = $this->morphy->getPartOfSpeech($word);

                        $updated_forms = $forms;

                        foreach ($forms as $form) {

                            $fPoS = $this->morphy->getPartOfSpeech($form);
                            $formWord = mb_strtolower($form, "UTF-8");

                            if ($this->lang == "rus") {
                                //crazy russian morphology
                                //trying to add some modifications to verbs

                                if ((in_array('Г', $fPoS) || in_array('ИНФИНИТИВ', $fPoS))) {

                                    if ($this->endsWith($formWord, 'ть'))
                                        $updated_forms[] = $form . 'СЯ';

                                    if ($this->endsWith($formWord, 'тить'))
                                        $updated_forms[] = str_replace('ТИТЬ', 'ЩАТЬ', $form);

                                    if ($this->endsWith($formWord, 'ся'))
                                        $updated_forms[] = mb_substr($form, 0, strlen($form) - 2, 'UTF-8');

                                    if ($this->endsWith($formWord, 'ить'))
                                        $updated_forms[] = str_replace('ИТЬ', 'АТЬ', $form);

                                    if ($this->endsWith($formWord, 'ить'))
                                        $updated_forms[] = str_replace('ИТЬ', 'ИВАТЬ', $form);

                                    if ($this->endsWith($formWord, 'ать'))
                                        $updated_forms[] = str_replace('АТЬ', 'ИТЬ', $form);

                                    if ($this->endsWith($formWord, 'ивать'))
                                        $updated_forms[] = str_replace('ИВАТЬ', 'ИТЬ', $form);

                                }
                            }
                        }

                        //adding data to result array
                        if (mb_strtolower($words[$row][$i], "UTF-8") == mb_strtolower($word, "UTF-8")) {

                            $finalWords[$row][$i] = array(
                                'word' => $this->lang == 'ru' ? mb_strtolower($words[$row][$i], 'UTF-8') : $words[$row][$i],
                                'data' => array(
                                    'forms' => $updated_forms,
                                    'partOfSpeech' => $pOS
                                )
                            );

                            if ($this->lang == 'rus' && mb_strtolower($words[$row][$i], "UTF-8") == 'только')
                                $partOfSpeech = array('Н');
                            else
                                $partOfSpeech = $pOS;

                            $is_important = false;

                            if (is_array($partOfSpeech)) {

                                foreach ($this->allowedPoS as $aPos) {
                                    if (in_array($aPos, $partOfSpeech)) {
                                        $is_important = true;
                                        break;
                                    }
                                }

                                foreach ($this->restrictedPoS as $rPos) {
                                    if (in_array($rPos, $partOfSpeech)) {
                                        $is_important = false;
                                        break;
                                    }
                                }

                                if ($this->checkForStopWords && in_array(strtolower($words[$row][$i]), $this->stopWordsList[$this->lang]))
                                    $is_important = false;

                                if ($this->lang == 'eng' && (mb_strtolower($words[$row][$i], "UTF-8") == 'would' || mb_strtolower($words[$row][$i], "UTF-8") == 'not'))
                                    $is_important = false;

                                if ($is_important)
                                    $importantCounter++;

                            }

                            $finalWords[$row][$i]['is_important'] = $is_important;
                        }

                    }

                }

            }

            return array('final_words' => $finalWords, 'importantCounter' => $importantCounter);


        } catch (phpMorphy_Exception $e) {
            die('Error occured while text processing: ' . $e->getMessage());
        }
    }
}