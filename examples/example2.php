<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Test</title>

</head>
<body>
<?php
error_reporting(E_ALL | E_STRICT);

include("db.php");
$db = new DB();
$r = $db->searchWord('there');
// first we include phpmorphy library
require_once(dirname(__FILE__) . '/../src/common.php');

// set some options
$opts = array(
	// storage type, follow types supported
	// PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
	// PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
	// PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
	'storage' => PHPMORPHY_STORAGE_FILE,
	// Extend graminfo for getAllFormsWithGramInfo method call
	'with_gramtab' => false,
	// Enable prediction by suffix
	'predict_by_suffix' => true,
	// Enable prediction by prefix
	'predict_by_db' => true
);

// Path to directory where dictionaries located
$dir = dirname(__FILE__) . '/../dicts';

// Create descriptor for dictionary located in $dir directory with russian language
$dict_bundle = new phpMorphy_FilesBundle($dir, 'eng');

// Create phpMorphy instance
try {
	$morphy = new phpMorphy($dict_bundle, $opts);
} catch(phpMorphy_Exception $e) {
	die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
}

// All words in dictionary in UPPER CASE, so don`t forget set proper locale
// Supported dicts and locales:
//  *------------------------------*
//  | Dict. language | Locale name |
//  |------------------------------|
//  | Russian        | cp1251      |
//  |------------------------------|
//  | English        | cp1250      |
//  |------------------------------|
//  | German         | cp1252      |
//  *------------------------------*
// $codepage = $morphy->getCodepage();
// setlocale(LC_CTYPE, array('ru_RU.CP1251', 'Russian_Russia.1251'));


$text = "There will always be this place
inside
where I feel her absence
where I feel the echo of her lost voice –
the one she would have used to call me
back from sadness as she had to be
called so many times back from madness.

What would it take to summon her –
Not having an address, just a marker
for where she is not
I can only go and visit
her absence her remains
which become less and less like her
more and more like the earth and trees,
the sky she continually faces.
I’d rather picture her under the sea
hair waving to the fishes and the brine,
being washed clean by
sharks and plankton
than under those pines by
the stone bench: one more desiccating root
in a garden of bones.";



//echo "Testing single mode...\n";
?>

<style>
.word{font-size: 20px;}
.data{font-size: 12px; background-color: yellow; display: inline-block; padding:3px; text-transform: lowercase;}
.nonimportant { background-color: red;}
</style>
<?


function getTextBetweenTags($string, $tagname)
 {
    $pattern = "/<$tagname>(.*?)<\/$tagname>/";
    preg_match_all($pattern, $string, $matches);
    return $matches[1];
 }
 //echo $r[0]['definition'];
$stt = preg_replace("/<c>.+?<\/c>/i", "", $r[0]['definition']);
$stt = preg_replace("/<co>.+?<\/co>/i", "", $r[0]['definition']);
	$stt =preg_replace("/\([^)]+\)/","",$stt);

$tg = getTextBetweenTags($stt, 'dtrn');

foreach ($tg as $t)
	echo $t."<br>";

$prep_list = array('aboard','about','above','across','after','against','along','amid','among','anti','around','as','at',
'before','behind','below','beneath','beside','besides','between','beyond','but','by',
'concerning','considering','despite',
'down','during',
'except','excepting','excluding',
'following','for','from',
'in','inside','into',
'like',
//'minus',
//'near',
'of','off','on','onto','opposite','outside','over',
'past','per','plus',
'regarding','round',
'save','since',
'than' ,'through','to','toward','towards',
'under','underneath','unlike',
'until','up','upon',
'versus','via',
'with','within','without');

function prepate_text($text){
	$res = str_replace('’', '\'', $text);
	$res = str_replace('\'d', ' would', $res);
	$res = str_replace('\'ll',' will', $res);
	$res = str_replace('wanna', 'want to', $res);
	$res = str_replace('gonna', 'going to', $res);

	return $res;
}
$text = prepate_text($text);
try {

	$words = array();
	$rows = explode("\n", $text);
	$rowNumber = 0;
	foreach($rows as $row){
		$words[$rowNumber] = array();
		$row = str_replace("ё", "е", $row);
		$list = str_word_count($row, 1, "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя");

		$index = 0;
		foreach ($list as $word){
			$words[$rowNumber][$index] = $word;
			$index++;
		}


		$rowNumber++;
	}



	$final_words = array();

	foreach ($words as $row=>$rowWords){
		$preparedWords = array();

		$final_words[$row] = array();

		foreach ($rowWords as $w){

			$preparedWords[] = mb_strtoupper($w,  "UTF-8");

		}

		$base_form = $morphy->getBaseForm($preparedWords);



		foreach ($base_form as $word => $forms){

			if ($word == '-')
				continue;



			for ($i = 0; $i < count($words[$row]); $i++){
				if ( mb_strtolower($words[$row][$i],  "UTF-8") == mb_strtolower($word,  "UTF-8") )
				$final_words[$row][$i] = array(
					'word' => $words[$row][$i],
					'data' => array(
						$forms,
						$morphy->getPartOfSpeech($word)
					)
				);
			}

			//echo $word . "(". $forms_str  . '; ' . $pOS_string . ") ";
		}


		//echo "<br>";
		//$all_forms = $morphy->getAllForms($bulk_words);
	}

	$important = 0;
	foreach ($final_words as $row => $words)
	{
		ksort($words);
		foreach ($words as $word){
			$forms_str = is_array($word['data'][0]) && count($word['data'][0]) > 0 ? implode(',', $word['data'][0]) : '?';

			$partOfSpeech = $word['data'][1];

			$pOS_string = is_array($partOfSpeech) && count($partOfSpeech) > 0 ? implode(',', $partOfSpeech) : '?';
			$is_important = false;
			if (is_array($partOfSpeech)){
				//print_r($partOfSpeech);
				if ( (in_array('ADVERB', $partOfSpeech) ||  in_array('ADJECTIVE', $partOfSpeech) || in_array('PN', $partOfSpeech) || in_array('NOUN', $partOfSpeech)
					|| in_array('VERB', $partOfSpeech) 			|| in_array('PN_ADJ', $partOfSpeech)
				)
				 && !in_array(strtolower($word['word']), $prep_list  )
				){
					$important++;
					$is_important = true;

					/*
					 && (
					!in_array('СОЮЗ',$partOfSpeech ) && !in_array('ПРЕДЛ',$partOfSpeech ) && !in_array('МЕЖД',$partOfSpeech )
				)
					*/
				}
			}

			if ($is_important)
				$w = '<u>' . $word['word'] . '</u>';
			else
				$w = '<span class="nonimportant">' . $word['word'] . '</span>';

			echo '<span class="word">' . $w . "<span class='data'>(". $forms_str  . '; ' . $pOS_string . ")</span> ";
		}
		echo "<br>";


	}

	echo "<hr /> Знаменательных слов:" . $important;

} catch(phpMorphy_Exception $e) {
	die('Error occured while text processing: ' . $e->getMessage());
}
?>
</body>
</html>