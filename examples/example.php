<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Test</title>

</head>
<body>
<?php
error_reporting(E_ALL | E_STRICT);

// first we include phpmorphy library
require_once(dirname(__FILE__) . '/../src/common.php');

// set some options
$opts = array(
	// storage type, follow types supported
	// PHPMORPHY_STORAGE_FILE - use file operations(fread, fseek) for dictionary access, this is very slow...
	// PHPMORPHY_STORAGE_SHM - load dictionary in shared memory(using shmop php extension), this is preferred mode
	// PHPMORPHY_STORAGE_MEM - load dict to memory each time when phpMorphy intialized, this useful when shmop ext. not activated. Speed same as for PHPMORPHY_STORAGE_SHM type
	'storage' => PHPMORPHY_STORAGE_FILE,
	// Extend graminfo for getAllFormsWithGramInfo method call
	'with_gramtab' => false,
	// Enable prediction by suffix
	'predict_by_suffix' => true, 
	// Enable prediction by prefix
	'predict_by_db' => true
);

// Path to directory where dictionaries located
$dir = dirname(__FILE__) . '/../dicts';

// Create descriptor for dictionary located in $dir directory with russian language
$dict_bundle = new phpMorphy_FilesBundle($dir, 'rus');

// Create phpMorphy instance
try {
	$morphy = new phpMorphy($dict_bundle, $opts);
} catch(phpMorphy_Exception $e) {
	die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
}

// All words in dictionary in UPPER CASE, so don`t forget set proper locale
// Supported dicts and locales:
//  *------------------------------*
//  | Dict. language | Locale name |
//  |------------------------------|
//  | Russian        | cp1251      |
//  |------------------------------|
//  | English        | cp1250      |
//  |------------------------------|
//  | German         | cp1252      |
//  *------------------------------*
// $codepage = $morphy->getCodepage();
// setlocale(LC_CTYPE, array('ru_RU.CP1251', 'Russian_Russia.1251'));

// Hint: in this example words $word_one, $word_two are in russian language(cp1251 encoding)
$word_one = 'ПРОВЕРКА';
$word_two = 'МОРФОРЛОГИЗАТОРА';

$text = "Всегда будет это место
внутри
где я чувствую ее отсутствие
где я чувствую эхо её пропавшего голоса – 
одного, которым она раньше звала меня
назад от печали так, как её 
звали так много раз назад от безумия.

Что потребовалось бы, чтобы её вызвать – 
Не имея адреса, только знак,
для указания, где её нет
Я могу только пойти и посетить
Её отсутствие, её остатки,
которые становятся всё меньше и меньше похожие на неё,
все больше и больше похожими на землю и деревья,
небо, куда она непрерывно повернута.
Я бы лучше рисовала её под толщей моря,
с волосами, развевающимися среди рыб и рассола, 
будучи помытой чисто
акулами и планктоном 
чем под теми соснами
у каменной скамьи: еще один высушенный корень
в саду костей.";

//echo "Testing single mode...\n";
?>

<style>
.word{font-size: 20px;}
.data{font-size: 12px; background-color: yellow; display: inline-block; padding:3px; text-transform: lowercase;}
.nonimportant { background-color: red;}
</style>
<?
try {
	// word by word processing
	// each function return array with result or FALSE when no form(s) for given word found(or predicted)
	//$base_form = $morphy->getBaseForm($word_one);
	//$all_forms = $morphy->getAllForms($word_one);
	//$pseudo_root = $morphy->getPseudoRoot($word_one);
	
	//if(false === $base_form || false === $all_forms || false === $pseudo_root) {
	//	die("Can`t find or predict $word_one word");
	//}
	
	//echo 'base form = ' . implode(', ', $base_form) . "\n";
	//echo 'all forms = ' . implode(', ', $all_forms) . "\n";
	
	//echo "Testing bulk mode...\n";
	$words = array();
	$rows = explode("\n", $text);
	$rowNumber = 0;
	foreach($rows as $row){
		$words[$rowNumber] = array();
		$row = str_replace("ё", "е", $row);
		$list = str_word_count($row, 1, "АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя");
		
		$index = 0;
		foreach ($list as $word){
			$words[$rowNumber][$index] = $word;
			$index++;
		}
		
		
		$rowNumber++;
	}
	
	
	
	$final_words = array();
	
	foreach ($words as $row=>$rowWords){
		$preparedWords = array();
		
		$final_words[$row] = array();
		
		foreach ($rowWords as $w){
		
			$preparedWords[] = mb_strtoupper($w,  "UTF-8");
			
		}
	
		$base_form = $morphy->getBaseForm($preparedWords);
		
		
		
		foreach ($base_form as $word => $forms){
			
			if ($word == '-')
				continue;
				
			
			
			for ($i = 0; $i < count($words[$row]); $i++){
				if ( mb_strtolower($words[$row][$i],  "UTF-8") == mb_strtolower($word,  "UTF-8") )
				$final_words[$row][$i] = array(
					'word' => $words[$row][$i],
					'data' => array(
						$forms, 
						$morphy->getPartOfSpeech($word)
					)
				);
			}
			
			//echo $word . "(". $forms_str  . '; ' . $pOS_string . ") ";
		}
		
		
		//echo "<br>";
		//$all_forms = $morphy->getAllForms($bulk_words);
	}
	
	$important = 0;
	foreach ($final_words as $row => $words)
	{
		ksort($words);
		foreach ($words as $word){
			$forms_str = is_array($word['data'][0]) && count($word['data'][0]) > 0 ? implode(',', $word['data'][0]) : '?';
			
			if ( mb_strtolower($word['word'], "UTF-8") == 'только')
				$partOfSpeech = array('Н');
			else
				$partOfSpeech = $word['data'][1];

			$pOS_string = is_array($partOfSpeech) && count($partOfSpeech) > 0 ? implode(',', $partOfSpeech) : '?';
			$is_important = false;
			if (is_array($partOfSpeech)){
				
				if ( (in_array('Г', $partOfSpeech) ||  in_array('КР_ПРИЧАСТИЕ', $partOfSpeech) || in_array('ИНФИНИТИВ', $partOfSpeech) || in_array('МС-П', $partOfSpeech) ||
				in_array('Н', $partOfSpeech) || in_array('ЧИСЛ', $partOfSpeech) || in_array('П', $partOfSpeech) || in_array('МС', $partOfSpeech) || 
				in_array('С', $partOfSpeech) || in_array('ДЕЕПРИЧАСТИЕ', $partOfSpeech) || in_array('ПРИЧАСТИЕ', $partOfSpeech))
				
				 && (
					!in_array('СОЮЗ',$partOfSpeech ) && !in_array('ПРЕДЛ',$partOfSpeech ) && !in_array('МЕЖД',$partOfSpeech ) 
				) 
				
				){
					$important++;
					$is_important = true;
					
					/*
					 && (
					!in_array('СОЮЗ',$partOfSpeech ) && !in_array('ПРЕДЛ',$partOfSpeech ) && !in_array('МЕЖД',$partOfSpeech ) 
				) 
					*/
				}
			}
			
			if ($is_important)
				$w = '<u>' . $word['word'] . '</u>';
			else 
				$w = '<span class="nonimportant">' . $word['word'] . '</span>';
			
			echo '<span class="word">' . $w . "<span class='data'>(". $forms_str  . '; ' . $pOS_string . ")</span> ";
		}
		echo "<br>";
		
		
	}
	
	echo "<hr /> Знаменательных слов:" . $important;
	
	
	
	// bulk mode speed-ups processing up to 50-100%(mainly for getBaseForm method)
	// in bulk mode all function always return array
	//$bulk_words = array($word_one, $word_two);
	//$base_form = $morphy->getBaseForm($bulk_words);
	//$all_forms = $morphy->getAllForms($bulk_words);
	//$pseudo_root = $morphy->getPseudoRoot($bulk_words);
	
	// Bulk result format:
	// array(
	//   INPUT_WORD1 => array(OUTWORD1, OUTWORD2, ... etc)
	//   INPUT_WORD2 => FALSE <-- when no form for word found(or predicted) 
	// )
	//echo 'bulk mode base form = ' . implode(', ', $base_form[$word_one]) . ' ' . implode(', ', $base_form[$word_two]) . "\n";
	//echo 'bulk mode all forms = ' . implode(', ', $all_forms[$word_one]) . ' ' . implode(', ', $all_forms[$word_two]) . "\n";
	
	// You can also retrieve all word forms with graminfo via getAllFormsWithGramInfo method call
	// $all_forms_with_gram = $morphy->getAllFormsWithGramInfo($word_one);
} catch(phpMorphy_Exception $e) {
	die('Error occured while text processing: ' . $e->getMessage());
}
?>
</body>
</html>